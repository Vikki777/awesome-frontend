import { ICarouselDesc } from "./carousel-desc.modal";
export interface IShoppingCartItem extends ICarouselDesc {
  _cartId: string;
}
