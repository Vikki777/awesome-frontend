export interface ICarouselDesc {
  _id: string;
  title: string;
  imagePath: string;
  qty: number;
  isChecked: boolean;
  price: number;
}
