export interface ICartRequest {
  productId: string;
  isChecked: boolean;
  quantity: number;
  title: string;
  price: number;
  imagePath: string;
}
