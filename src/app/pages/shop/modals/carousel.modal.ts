import { ICarouselDesc } from "./carousel-desc.modal";
import { IError } from "../../../shared/modals";

export interface ICarousel {
  header?: string;
  err?: IError;
  carouselDesc?: ICarouselDesc[];
}
