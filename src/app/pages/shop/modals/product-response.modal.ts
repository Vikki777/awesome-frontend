import { IHeaderResponse } from "../../../shared/modals";
export interface IProductResponse {
  header: IHeaderResponse;
  products: IProduct[];
}

export interface IProduct {
  _id: string;
  title: string;
  price: number;
  imagePath: string;
}
