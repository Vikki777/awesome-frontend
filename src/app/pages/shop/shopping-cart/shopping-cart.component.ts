import { Component, OnInit } from "@angular/core";
import { ICarouselDesc, IShoppingCartItem } from "../modals";
import { CartService } from "../cart.service";

@Component({
  selector: "app-shopping-cart",
  templateUrl: "./shopping-cart.component.html",
  styleUrls: ["./shopping-cart.component.scss"],
})
export class ShoppingCartComponent implements OnInit {
  cartItems: IShoppingCartItem[] = [];
  constructor(private cartService: CartService) {}

  ngOnInit(): void {
    this.cartService.getCardObservable().subscribe((d) => {
      this.cartItems = [...d];
      console.log(d);
    });
  }

  getTotal(): number {
    const total: number = this.cartItems.reduce((acc, cur) => {
      if (cur.isChecked) {
        acc = acc + cur.price * cur.qty;
      }
      return acc;
    }, 0);
    return total;
  }

  getSeletedItemCount(): number {
    const totalSeleted: number = this.cartItems.filter((d) => d.isChecked)
      .length;
    return totalSeleted;
  }
}
