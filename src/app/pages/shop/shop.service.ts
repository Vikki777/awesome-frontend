import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { API } from "../../shared/configs";
import { IProductResponse, ICarousel } from "./modals";
import { map, delay } from "rxjs/operators";
import { ProductApiAdapter } from "./adapters";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ShopService {
  constructor(private http: HttpClient) {}

  getProductDetail(): Observable<ICarousel> {
    return this.http.get<IProductResponse>(API.GET_Products).pipe(
      map((d) => {
        return ProductApiAdapter.to(d);
      }),
      delay(300)
    );
  }
}
