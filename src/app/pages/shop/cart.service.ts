import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { ICarouselDesc, IShoppingCartItem, ICartRequest } from "./modals";
import { HttpClient } from "@angular/common/http";
import { API } from "../../shared/configs";
import { IPostResponse } from "../../shared/modals";
import { map } from "rxjs/operators";
@Injectable({
  providedIn: "root",
})
export class CartService {
  private cardItems: IShoppingCartItem[] = [];
  private cardItemObservable = new BehaviorSubject<IShoppingCartItem[]>([]);
  constructor(private http: HttpClient) {
    const localStogateData = localStorage.getItem("cardItems");
    if (localStogateData) {
      this.cardItems = JSON.parse(localStogateData);
      this.cardItemObservable.next([...this.cardItems]);
    }
  }

  private save(items: IShoppingCartItem[]) {
    localStorage.setItem("cardItems", JSON.stringify(items));
    this.cardItemObservable.next(items);
  }

  saveCardItem(item: ICarouselDesc): Observable<boolean> {
    const cartItem: ICartRequest = {
      productId: item._id,
      isChecked: true,
      quantity: 1,
      title: item.title,
      price: item.price,
      imagePath: item.imagePath,
    };
    return this.http.post<IPostResponse>(API.POST_Cart, cartItem).pipe(
      map((d) => {
        if (!d.header.error) {
          return true;
        } else {
          return false;
        }
      })
    );
  }

  getCardObservable(): Observable<IShoppingCartItem[]> {
    return this.cardItemObservable.asObservable();
  }

  updateIsChecked(id: string) {
    this.cardItems = [...this.cardItems].map((d) => {
      if (d._cartId === id) {
        d.isChecked = !d.isChecked;
      }
      return d;
    });
    this.save(this.cardItems);
  }

  deleteCartItem(id: string) {
    this.cardItems = [...this.cardItems].filter((d) => d._cartId !== id);
    this.save(this.cardItems);
  }

  updateCartQty(_id, itemQty: number) {
    this.cardItems = [...this.cardItems].map((d) => {
      if (d._cartId === _id) {
        d.qty = itemQty;
      }
      return d;
    });
    this.save(this.cardItems);
  }
}
