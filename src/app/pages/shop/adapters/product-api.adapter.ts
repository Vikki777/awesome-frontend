import {
  IProductResponse,
  ICarousel,
  IProduct,
  ICarouselDesc,
} from "../modals";
import { IError } from "../../../shared/modals/error.modal";
export class ProductApiAdapter {
  static toArray(d: IProduct): ICarouselDesc {
    const carouselData: ICarouselDesc = {
      _id: d._id,
      title: d.title,
      imagePath: d.imagePath,
      qty: 1,
      isChecked: true,
      price: d.price,
    };
    return carouselData;
  }
  static to(data: IProductResponse): ICarousel {
    if (!data.header.error) {
      return {
        header: "Flass Sale on Electronics - Recommended Items",
        carouselDesc: data.products.map((d) => this.toArray(d)),
      };
    } else {
      const error: IError = {
        errNo: data.header.error.errorNo,
        errDesc: data.header.error.errorDesc,
      };
      return { err: error };
    }
  }
}
