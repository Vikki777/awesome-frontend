import { Component, OnInit } from "@angular/core";
import { ICarouselDesc, ICarousel } from "../modals";
import { CartService } from "../cart.service";
import { ShopService } from "../shop.service";

@Component({
  selector: "app-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"],
})
export class ShopComponent implements OnInit {
  electronicItems: ICarousel;
  constructor(
    private cartService: CartService,
    private shopService: ShopService
  ) {}

  ngOnInit(): void {
    this.shopService.getProductDetail().subscribe((d) => {
      this.electronicItems = d;
    });
  }

  cardItem(item: ICarouselDesc) {
    this.cartService.saveCardItem(item).subscribe();
  }
}
