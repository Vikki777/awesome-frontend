import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { IShoppingCartItem } from "../modals";
import { CartService } from "../cart.service";

@Component({
  selector: "app-shopping-cart-item",
  templateUrl: "./shopping-cart-item.component.html",
  styleUrls: ["./shopping-cart-item.component.scss"],
})
export class ShoppingCartItemComponent implements OnInit {
  @Input() cartItem: IShoppingCartItem;
  constructor(private cartService: CartService) {}

  ngOnInit(): void {}

  selectedItem() {
    this.cartService.updateIsChecked(this.cartItem._cartId);
  }

  deleteItem() {
    this.cartService.deleteCartItem(this.cartItem._cartId);
  }

  resetCount(quanties: number) {
    this.cartService.updateCartQty(this.cartItem._cartId, quanties);
  }
}
