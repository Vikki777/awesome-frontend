import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ShopRoutingModule } from "./shop-routing.module";
import { ShopComponent } from "./shop/shop.component";
import { SharedModule } from "../../shared/shared.module";
import { AboutUsComponent } from "./about-us/about-us.component";
import { TrackerComponent } from "./tracker/tracker.component";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { ShoppingCartItemComponent } from "./shopping-cart-item/shopping-cart-item.component";
import { ShoppingCartComponent } from "./shopping-cart/shopping-cart.component";
import { CarouselImageComponent } from "./carousel-image/carousel-image.component";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";

@NgModule({
  declarations: [
    ShopComponent,
    AboutUsComponent,
    TrackerComponent,
    ContactUsComponent,
    ShoppingCartItemComponent,
    ShoppingCartComponent,
    CarouselImageComponent,
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    SharedModule,
    BsDropdownModule.forRoot(),
  ],
  exports: [CarouselImageComponent],
})
export class ShopModule {}
