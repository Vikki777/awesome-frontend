import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { FeatherModule } from "angular-feather";
import {
  ChevronRight,
  ChevronLeft,
  CheckSquare,
  Square,
} from "angular-feather/icons";

const icons = {
  ChevronLeft,
  ChevronRight,
  CheckSquare,
  Square,
};

@NgModule({
  declarations: [],
  imports: [CommonModule, FeatherModule.pick(icons)],
  exports: [FeatherModule],
})
export class IconsModule {}
