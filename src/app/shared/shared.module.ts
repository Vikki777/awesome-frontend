import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IconsModule } from "./modules/icons/icons.module";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [NavbarComponent],
  imports: [CommonModule, IconsModule, RouterModule],
  exports: [IconsModule, NavbarComponent],
})
export class SharedModule {}
