const BASE_URL = "http://localhost:3000/";

export const API = {
  GET_Products: BASE_URL + "awesome-ecommerce/products",
  POST_Cart: BASE_URL + "awesome-ecommerce/cart"
};
