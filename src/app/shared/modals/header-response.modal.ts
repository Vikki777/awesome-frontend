import { IErrorResponse } from "./error-response.modal";
export interface IHeaderResponse {
  method: string;
  error?: IErrorResponse;
}
