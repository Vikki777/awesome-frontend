export interface IErrorResponse {
  errorNo: number;
  errorDesc: string;
}
