import { IHeaderResponse } from "./header-response.modal";
export interface IPostResponse {
  header: IHeaderResponse;
  success: boolean;
}
 