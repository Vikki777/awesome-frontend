export interface IError {
  errNo: number;
  errDesc: string;
}
