export { IErrorResponse } from "./error-response.modal";
export { IError } from "./error.modal";
export { IHeaderResponse } from "./header-response.modal";
export { IPostResponse } from "./post-response.modal";